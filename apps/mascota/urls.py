from django.conf.urls import url, include
from apps.mascota.views import index, mascota_view, mascotalista, mascotaeditar, mascotaborrar

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^nuevo$', mascota_view, name='mascota_crear'),
    url(r'^lista$', mascotalista, name='mascota_lista'),
    url(r'^editar/(?P<id_mascota>\d+)/$', mascotaeditar, name='mascota_editar'),
    url(r'^eliminar/(?P<id_mascota>\d+)/$', mascotaborrar, name='mascota_borrar'),
]
