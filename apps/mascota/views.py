from django.shortcuts import render, redirect
from django.http import HttpResponse
from apps.mascota.form import MascotaForm
from apps.mascota.models import Mascota

# Create your views here.
def index(request):
    return render(request, 'mascota/index.html')

def mascota_view(request):
    if request.method == 'POST':
        form = MascotaForm(request.POST)
        if  form.is_valid():
            form.save()
        return redirect('mascota:index')
    else:
        form = MascotaForm()
        return render(request, 'mascota/formulario.html', {'form':form})

def mascotalista(request):
    mascota = Mascota.objects.all()
    contexto = {'mascotas':mascota}
    return render(request, 'mascota/lista_mascota.html', contexto)

def mascotaeditar(request, id_mascota):
    mascota = Mascota.objects.get(id=id_mascota)
    if request.method == 'GET':
        form = MascotaForm(instance=mascota)
    else:
        form = MascotaForm(request.POST, instance=mascota)
        if form.is_valid():
            form.save()
        return redirect('mascota:mascota_lista')
    return render(request, 'mascota/formulario.html', {'form':form})

def mascotaborrar(request, id_mascota):
    mascota = Mascota.objects.get(id=id_mascota)
    if request.method == 'POST':
        mascota.delete()
        return redirect('mascota:mascota_lista')
    return render(request, 'mascota/borrar_mascota.html', {'mascota':mascota})