from django import forms
from apps.mascota.models import Mascota


class MascotaForm(forms.ModelForm):
    class Meta:
        model = Mascota

        fields = [
            'imagen',
            'nombre',
            'raza',
            'descripcion',
            'estado',
            'persona',
        ]
        labels = {
            'imagen': 'Imagen',
            'nombre': 'Nombre',
            'raza': 'Raza',
            'descripcion': 'Descripción',
            'estado': 'Estado',
            'persona': 'Persona',

        }
        widgets = {
            'imagen': forms.FileInput(attrs={'class':'form-control'}),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'raza': forms.Select(attrs={'class':'form-control'}),
            'descripcion': forms.Textarea(attrs={'class':'form-control'}),
            'estado': forms.Select(attrs={'class':'form-control'}),
            'persona': forms.Select(attrs={'class':'form-control'}),
        }
