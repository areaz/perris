# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2018-10-30 15:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mascota', '0009_auto_20181030_1546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mascota',
            name='imagen',
            field=models.ImageField(blank=True, upload_to='static/img/rescatados'),
        ),
    ]
