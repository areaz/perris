from django.db import models
from apps.adopcion.models import Persona


# Create your models here.
class Raza(models.Model):
    nombre = models.CharField(max_length=50)


RAZSELEC = (
    ('desconocida', 'Desconocida'),
    ('raza1', 'Raza1'),
    ('raza2', 'Raza2'),
    ('raza3', 'Raza3'),
)

ESTSELEC = (
    ('disponible', 'Disponible'),
    ('adoptado', 'Adoptado')
)

class Mascota(models.Model):
    imagen = models.ImageField(upload_to='static/img/rescatados', blank=True)
    nombre = models.CharField(max_length=50)
    raza = models.CharField(max_length=20, choices=RAZSELEC, default='desconocida')
    descripcion = models.TextField(blank=True)
    estado = models.CharField(max_length=20, choices=ESTSELEC, default='adoptado')
    persona = models.ForeignKey(Persona, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.nombre)

