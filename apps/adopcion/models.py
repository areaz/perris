from django.db import models

# Create your models here.

class Persona(models.Model):
    rut= models.CharField(max_length=10)
    nombre = models.CharField(max_length=50)
    apellidop = models.CharField(max_length=50)
    apellidom = models.CharField(max_length=50)
    edad = models.IntegerField()
    telefono = models.CharField(max_length=12)
    email = models.EmailField()
    direccion = models.TextField()

    def __str__(self):
        return '{}'.format(self.nombre + ' ' + self.apellidop + ' ' + self.apellidom)